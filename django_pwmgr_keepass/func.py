from collections import deque
from io import BytesIO

import pysvn
from construct import ChecksumError
from django.conf import settings
from pykeepass import PyKeePass
from pykeepass.exceptions import CredentialsIntegrityError
from pykeepass.kdbx_parsing import KDBX

from django_pwmgr_models.django_pwmgr_models.models import PwmgrDbs


class OneAttemptLogin:
    """
    if login failed, pysvn goes into infinite loop.
    this class is used as callback; it allows only one login attempt
    source + (c): https://stackoverflow.com/a/41612332
    """

    def __init__(self, username, pwd):
        self.attempt_tried = False
        self.username = username
        self.pwd = pwd

    def __call__(self, x, y, z):
        if not self.attempt_tried:
            self.attempt_tried = True
            return (True, self.username, self.pwd, False)
        else:
            return (False, "xx", "xx", False)


class PwMgrPyKeePass(PyKeePass):
    def __init__(self, filename, **kwargs):
        super().__init__(filename, **kwargs)

    def read(self, filename=None, password=None, keyfile=None, transformed_key=None):
        self.password = password
        self.keyfile = keyfile
        if not filename:
            filename = self.filename

        try:
            self.kdbx = KDBX.parse_stream(
                filename,
                password=password,
                keyfile=keyfile,
                transformed_key=transformed_key,
            )
        except ChecksumError:
            raise CredentialsIntegrityError

    def save(self, filename=None, transformed_key=None):
        if not filename:
            filename = self.filename

        filename.seek(0)
        filename.write(
            KDBX.build(
                self.kdbx,
                password=self.password,
                keyfile=self.keyfile,
                transformed_key=transformed_key,
            )
        )
        filename.truncate()


def load_keepass_db(pwmgr_db: PwmgrDbs):
    pwmgr_file = BytesIO()

    svn_client = pysvn.Client()
    svn_client.callback_get_login = OneAttemptLogin(
        settings.SVN_USERNAME, settings.SVN_PASSWORD
    )
    svn_client.set_auth_cache(False)
    svn_client.set_store_passwords(False)
    svn_client.set_default_username("")
    svn_client.set_default_password("")
    svn_client.set_interactive(False)

    pwmgr_file.write(svn_client.cat("http://192.168.81.4/svn/DEV_TEST/test.kdbx"))
    pwmgr_file.seek(0)

    kp = PwMgrPyKeePass(pwmgr_file, password=pwmgr_db.db_master_pw)

    return kp


def save_keepass_db(pwmgr_db: PwmgrDbs, pw_manager: PwMgrPyKeePass):
    svn_client = pysvn.Client()
    svn_client.callback_get_login = OneAttemptLogin(
        settings.SVN_USERNAME, settings.SVN_PASSWORD
    )
    svn_client.set_auth_cache(False)
    svn_client.set_store_passwords(False)
    svn_client.set_default_username("")
    svn_client.set_default_password("")
    svn_client.set_interactive(False)

    svn_client.import_(
        pw_manager.filename.getvalue(),
        "http://192.168.81.4/svn/DEV_TEST/test.kdbx",
        "Update Credentials",
    )

    return True


def traverse_keepass_tree(kp: PyKeePass, cred_path, group=None):
    path_split = deque(cred_path.split("/"))
    path_split_len = len(path_split)
    current_path_level = path_split.popleft()

    if group is None:
        current_group = kp.find_groups(name=current_path_level, first=True)
    else:
        current_group = kp.find_groups(name=current_path_level, group=group, first=True)

    if current_group is None:
        if group is None:
            current_group = kp.add_group(kp.root_group, current_path_level)
        else:
            current_group = kp.add_group(group, current_path_level)

    if path_split_len == 1:
        return current_group
    else:
        return traverse_keepass_tree(kp, "/".join(path_split), current_group)


def credential_helper(kp: PyKeePass, cred_title, cred_path):
    local_group = traverse_keepass_tree(kp, cred_path)
    entries = kp.find_entries(title=cred_title, group=local_group)

    entries_len = len(entries)

    if entries_len < 1:
        entry = kp.add_entry(local_group, cred_title, username="", password="")
    elif entries_len > 1:
        raise Exception("Found more than one entry for this path!")
    else:
        entry = entries[0]

    return entry
