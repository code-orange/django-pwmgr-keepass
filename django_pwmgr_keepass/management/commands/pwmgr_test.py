from django.core.management.base import BaseCommand

from django_pwmgr_keepass.django_pwmgr_keepass.func import *
from django_pwmgr_models.django_pwmgr_models.models import *


class Command(BaseCommand):
    help = "Test PWmgr"

    def handle(self, *args, **options):
        # load database
        db_meta = PwmgrDbs.objects.get(id=1)

        paths = gen_path(db_meta)

        repo = get_repo_for_db(db_meta)

        kp = load_keepass_db(db_meta)

        # find any group by its name
        cred = credential_helper(kp, "fritz@example.com", "Test/Hosted Exchange")

        cred.username = "test"
        cred.password = "secure"

        # save database
        kp.save()

        repo.git.add(paths["database_file"])
        repo.index.commit("Update password database")
        repo.git.push()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
